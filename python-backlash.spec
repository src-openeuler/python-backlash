%global _empty_manifest_terminate_build 0
Name:		python-backlash
Version:	0.3.2
Release:	1
Summary:	Standalone WebOb port of the Werkzeug Debugger with Python3 support meant to replace WebError in future TurboGears2
License:	MIT
URL:		https://github.com/TurboGears/backlash
Source0:	https://files.pythonhosted.org/packages/dc/b0/cb16bf831dbf45760ed477e36dcc9a577da082d2d3c84197a7caf4e961d7/backlash-0.3.2.tar.gz
BuildArch:	noarch


%global _description\
backlash is a swiss army knife for web applications debugging, which provides:\
    - An Interactive In Browser Debugger based on a Werkzeug Debugger fork ported to WebOb\
    - Crash reporting by email and on Sentry\
    - Slow requests reporting by email and on Sentry.\
Backlash was born as a replacement for WebError in TurboGears2.3 versions.
%description %_description

%package -n python3-backlash
Summary:	Standalone WebOb port of the Werkzeug Debugger with Python3 support meant to replace WebError in future TurboGears2
Provides:	python-backlash
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
%description -n python3-backlash %_description

%package help
Summary:	Development documents and examples for backlash
Provides:	python3-backlash-doc
%description help %_description

%prep
%autosetup -n backlash-%{version}

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-backlash -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Mon Jul 29 2024 Hann <hannannan@kylinos.cn> - 0.3.2-1
- Update package verison to 0.3.2
- Fix issue with utf8 exceptions

* Tue Jun 23 2020 Python_Bot <Python_Bot@openeuler.org>
- Package Spec generated
